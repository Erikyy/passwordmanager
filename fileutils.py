from os.path import expanduser
from os import path
import os
from configparser import ConfigParser
import json
class FileUtils():
    def __init__(self):
        self.config_parser = ConfigParser()

    def delete_file(self, _path, file):
        if path.exists(_path + file):
            os.remove(_path + file)
            
    def write_to_file(self, content, _path, file):
        with open(_path + file, "w") as out:
            json.dump(content, out)

    def read_json_file(self, _p, file):
        f_obj = open(_p + file)
        return json.load(f_obj)

    def read_file(self, _p, file) -> bytes:
        data = b""
        with open(_p + file, "rb") as f:
            data = f.read()
        return data

    def encrypt_file(self, _p, file, content):
        with open(_p + file, 'wb') as target:
            target.write(content)

    def decrypt_file(self, _p, file, content):
        with open(_p + file, 'wb') as target:
            target.write(content)

    def config_check(self):
        if not path.exists("./config.ini"):
            return False
        else:
            return True

    def read_type(self, _type, name):
        self.config_parser.read("./config.ini")
        type_name = self.config_parser[_type]
        return type_name[name]
    
    def create_type(self, _type, name, value):
        self.config_parser[_type] = {
            name: value
        }
        with open("./config.ini", "w") as conf:
            self.config_parser.write(conf)
    
    def change_type(self, _type, name, value):
        self.config_parser.read("./config.ini")
        type_name = self.config_parser[_type]
        type_name[name] = value
        with open("./config.ini", "w") as conf:
            self.config_parser.write(conf)

    def write_hash(self, key: bytes):
        with open("./hash.enc", "wb") as file:
            file.write(key)
    
    def read_hash(self):
        key = b''
        with open("./hash.enc", "rb") as file:
            key = file.read(256)
        return key
        
    def write_salt(self, salt: bytes):
        with open("./salt.enc", "wb") as file:
            file.write(salt)

    def read_salt(self) -> bytes:
        salt = b""
        with open("./salt.enc", "rb") as file:
            salt = file.read(256)
        return salt