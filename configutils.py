import os 
from fileutils import FileUtils

file_utils = FileUtils()
class ConfigUtils():

    def config_exists(self):
        if os.path.exists("./config.ini"):
            return True
        else:
            return False
            
    def saltExists(self):
        if os.path.exists("./salt.enc") and os.path.exists("./hash.enc"):
            return True
        else:
            return False

    def create_path(self, path):
        file_utils.create_type("PATH", "path", path)
    
    def read_path(self) -> str:
        return file_utils.read_type("PATH", "path")