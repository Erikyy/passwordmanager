const Test = {
    data() {
        return {
            users: [
                { 
                    name: '',
                    email: '',
                    mobno: '' 
                }
            ]
        }
    },
    methods: {
        addRow: function () {
            this.users.push({ name: '', email: '', mobno: '' })
        },
        deleteRow(index) {
            this.users.splice(index, 1);
        }
    }
}

Vue.createApp(Test).mount('#app')