package com.erik.pwmanager.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;


@Entity
@Table(name = "passwords")
public class Password {
    
    private int id;
    private String name;
    private String psw;
    private String iv;

    private User user;

    public Password() {}

    public Password(String name,String psw, String iv) {
        this.name = name;
        this.psw = psw;
        this.iv = iv;
    }

    @Id
    @GeneratedValue
    public int getId() {
        return id;
    }
    @Column(name = "name")
    public String getName() {
        return name;
    }

    @Column(name = "pass")
    public String getPsw() {
        return psw;
    }

    @Column(name = "iv")
    public String getIv() {
        return iv;
    }

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    public User getUser() {
        return user;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPsw(String psw) {
        this.psw = psw;
    }

    public void setIv(String iv) {
        this.iv = iv;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
