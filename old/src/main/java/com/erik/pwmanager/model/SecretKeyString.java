package com.erik.pwmanager.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SecretKeyString {

    private String key;

    public SecretKeyString(String key) {
        this.key = key;
    }
    
}
