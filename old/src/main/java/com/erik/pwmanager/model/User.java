package com.erik.pwmanager.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "users")
public class User {
    
    private int id;

    private String userName;

    private String password;

    private String role;

    private Set<Password> passwords;

    public User() {}

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    public int getId() {
        return id;
    }
    @Column(name = "user_name")
    public String getUserName() {
        return userName;
    }

    @Column(name = "password")
    public String getPassword() {
        return password;
    }
    
    public String getRole() {
        return role;
    }
    @JsonIgnore
    @OneToMany(mappedBy = "user")
    public Set<Password> getPasswords() {
        return passwords;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public void setPasswords(Set<Password> passwords) {
        this.passwords = passwords;
    }
}
