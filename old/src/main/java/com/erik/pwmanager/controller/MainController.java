package com.erik.pwmanager.controller;

import com.erik.pwmanager.services.SecurityService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {
    

    @Autowired
    private SecurityService securityService;


    @GetMapping("/")
    public String index() {
        return "index";
    }

    @GetMapping("/settings")
    public String settings() {
        return "settings";
    }

    @GetMapping("/login")
    public String login(Model model, String error, String logout) {
        if (securityService.isAuthenticated()) {
            return "redirect:/home";
        }

        if (error != null) {
            model.addAttribute("error", "Username and password is invalid");
        }

        if (logout != null) 
            model.addAttribute("message", "Logged out");
        return "login";
    }

    @GetMapping("/home")
    public String home(Model model) {
        return "home";
    }

    @GetMapping("/admin")
    public String admin(Model model) {
        return "admin";
    }
}
