package com.erik.pwmanager.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Set;

import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;

import com.erik.pwmanager.model.DbRepository;
import com.erik.pwmanager.model.Password;
import com.erik.pwmanager.model.SecretKeyString;
import com.erik.pwmanager.model.User;
import com.erik.pwmanager.services.UserService;
import com.erik.pwmanager.util.ECIESKey;
import com.google.gson.Gson;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
public class GraphController { 
    
    @Autowired
    private DbRepository repository;

    @Autowired
    private UserService service;

    Logger logger = LoggerFactory.getLogger(GraphController.class);

    
    @GetMapping("/generatekey")
    public String getKey() throws Exception {
        
        SecretKey key = ECIESKey.genSecretKey();
        String keyStr = Base64.getEncoder().encodeToString(key.getEncoded());
        Gson gson = new Gson();
        SecretKeyString keyString = new SecretKeyString(keyStr);
        return gson.toJson(keyString);
    }

    
    @GetMapping("/addpassword")
    public void addPassword(@RequestParam String name, @RequestParam String password, @RequestParam("key") String key, Principal principal) throws Exception{
        SecretKey secretKey = ECIESKey.getSecretKey(key.getBytes());
        User user = service.findUser(principal.getName());
        IvParameterSpec iv = ECIESKey.genIV();
        byte[] encrypt = ECIESKey.encrypt(password, secretKey, iv);
        byte[] encryptName = ECIESKey.encrypt(name, secretKey, iv);
        String str = Base64.getEncoder().encodeToString(encrypt);
        String nameStr = Base64.getEncoder().encodeToString(encryptName);
        String ivStr = Base64.getEncoder().encodeToString(iv.getIV());
        Password _password = new Password(nameStr, str, ivStr);
        _password.setUser(user);
        repository.save(_password);
    }

    
    @GetMapping("/passwords")
    public List<Password> getPasswords(@RequestParam String key, Principal principal) throws Exception {
        SecretKey secretKey = ECIESKey.getSecretKey(key.getBytes());
        User user = service.findUser(principal.getName());
        Set<Password> encrypted = user.getPasswords();
        List<Password> decrypted = new ArrayList<>();
        for(Password password : encrypted) {
            byte[] data = Base64.getDecoder().decode(password.getPsw());
            byte[] name = Base64.getDecoder().decode(password.getName());
            byte[] ivData = Base64.getDecoder().decode(password.getIv());
            IvParameterSpec ivParam = new IvParameterSpec(ivData);
            String decryptedPsw = ECIESKey.decrypt(data, secretKey, ivParam);
            String decryptedName = ECIESKey.decrypt(name, secretKey, ivParam);
            Password pass = new Password(decryptedName, decryptedPsw, password.getIv());
            pass.setId(password.getId());
            decrypted.add(pass);
        } 
        return decrypted;
    }
    
    @GetMapping("/remove")
    public void removePassword(@RequestParam int id, Principal principal) {
        User user = service.findUser(principal.getName());
        for (Password password : user.getPasswords()) {
            if (password.getUser().getId() == user.getId()) {
                repository.deleteById(id);
            }
        }
    }

    @GetMapping("/users")
    public List<User> getUsers() {
        return service.getAll();
    }

    @PostMapping("/adduser")
    public void addUser(@RequestParam String password, @RequestParam String userName, @RequestParam String role) {
        User user = new User();
        user.setRole(role);
        user.setPassword(password);
        user.setUserName(userName);
        service.save(user);
    }

    @PostMapping("/removeuser")
    public void removeUser(@RequestParam String userName) {
        User user = service.findUser(userName);
        service.delete(user);
    }
}