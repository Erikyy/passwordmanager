package com.erik.pwmanager.services;

import java.util.List;

import com.erik.pwmanager.model.User;

public interface UserService {
    
    void save(User user);
    void delete(User user);
    List<User> getAll();
    User findUser(String userName);
}
