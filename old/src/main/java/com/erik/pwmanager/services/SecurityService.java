package com.erik.pwmanager.services;

public interface SecurityService {
    boolean isAuthenticated();
}
