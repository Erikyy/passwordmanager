package com.erik.pwmanager.util;

import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ECIESKey {
    static Logger logger = LoggerFactory.getLogger(ECIESKey.class);
    private static String algorithm = "AES/CBC/PKCS5Padding";

    public static SecretKey genSecretKey() throws Exception {

        KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
        keyGenerator.init(new SecureRandom());
        SecretKey key = keyGenerator.generateKey();
        return key;
    }

    public static SecretKey getSecretKey(byte[] key) {
        SecretKey secretKey = new SecretKeySpec(key, "AES");
        return secretKey;
    }

    public static byte[] encrypt(String data, SecretKey key,IvParameterSpec iv) throws Exception{
        Cipher cipher = Cipher.getInstance(algorithm);
        cipher.init(Cipher.ENCRYPT_MODE, key, iv);
        byte[] encrypted = cipher.doFinal(data.getBytes());
        return encrypted;
    }

    public static String decrypt(byte[] data, SecretKey key, IvParameterSpec iv) throws Exception{
        Cipher cipher = Cipher.getInstance(algorithm);
        cipher.init(Cipher.DECRYPT_MODE, key, iv);
        String str = new String(cipher.doFinal(data));
        return str;
    }

    public static IvParameterSpec genIV() {
        byte[] iv = new byte[16];
        new SecureRandom().nextBytes(iv);
        return new IvParameterSpec(iv);
    }

}
