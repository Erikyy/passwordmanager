

class Stylesheets():

    def home(self):
        return """
        QPushButton{	
	        background-image: url(:/icons/icons/home.svg);
	        color: rgb(255,255,255);
	        background-color: rgb(28, 28, 28);
	        background-repeat: no-repeat;
	        background-position: left center;
	        border:none;
	        border-left: 22px solid rgba(28, 28, 28, 255);
	        text-align: left;
	        padding-left: 48px
        }
        QPushButton:hover {
	        background-color: rgb(154, 153, 150);
	        border-left: 22px solid rgba(154, 153, 150, 255);
        }"""
    def home_active(self):
        return """
        QPushButton{	
	        background-image: url(:/icons/icons/home.svg);
	        color: rgb(255,255,255);
	        background-color: rgb(60, 60, 60);
	        background-repeat: no-repeat;
	        background-position: left center;
	        border:none;
	        border-left: 22px solid rgba(60, 60, 60, 255);
	        text-align: left;
	        padding-left: 48px
        }
        QPushButton:hover {
	        background-color: rgb(154, 153, 150);
	        border-left: 22px solid rgba(154, 153, 150, 255);
        }"""
    def about(self):
        return """
        QPushButton{	
	        background-image: url(:/icons/icons/info.svg);
	        color: rgb(255,255,255);
	        background-color: rgb(28, 28, 28);
	        background-repeat: no-repeat;
	        background-position: left center;
	        border:none;
	        border-left: 22px solid rgba(28, 28, 28, 255);
	        text-align: left;
	        padding-left: 48px
        }
        QPushButton:hover {
	        background-color: rgb(154, 153, 150);
	        border-left: 22px solid rgba(154, 153, 150, 255);
        }"""
    def about_active(self):
        return """
        QPushButton{	
	        background-image: url(:/icons/icons/info.svg);
	        color: rgb(255,255,255);
	        background-color: rgb(60, 60, 60);
	        background-repeat: no-repeat;
	        background-position: left center;
	        border:none;
	        border-left: 22px solid rgba(60, 60, 60, 255);
	        text-align: left;
	        padding-left: 48px
        }
        QPushButton:hover {
	        background-color: rgb(154, 153, 150);
	        border-left: 22px solid rgba(154, 153, 150, 255);
        }"""
    def settings(self):
        return """
        QPushButton{	
	        background-image: url(:/icons/icons/setting.svg);
	        color: rgb(255,255,255);
	        background-color: rgb(28, 28, 28);
	        background-repeat: no-repeat;
	        background-position: left center;
	        border:none;
	        border-left: 22px solid rgba(28, 28, 28, 255);
	        text-align: left;
	        padding-left: 48px
        }
        QPushButton:hover {
	        background-color: rgb(154, 153, 150);
	        border-left: 22px solid rgba(154, 153, 150, 255);
        }"""
    def settings_active(self):
        return """
        QPushButton{	
	        background-image: url(:/icons/icons/setting.svg);
	        color: rgb(255,255,255);
	        background-color: rgb(60, 60, 60);
	        background-repeat: no-repeat;
	        background-position: left center;
	        border:none;
	        border-left: 22px solid rgba(60, 60, 60, 255);
	        text-align: left;
	        padding-left: 48px
        }
        QPushButton:hover {
	        background-color: rgb(154, 153, 150);
	        border-left: 22px solid rgba(154, 153, 150, 255);
        }"""