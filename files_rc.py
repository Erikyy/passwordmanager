# Resource object code (Python 3)
# Created by: object code
# Created by: The Resource Compiler for Qt version 5.15.2
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore

qt_resource_data = b"\
\x00\x00\x00\xf7\
<\
svg xmlns=\x22http:\
//www.w3.org/200\
0/svg\x22 height=\x222\
4\x22 viewBox=\x220 0 \
24 24\x22 width=\x2224\
\x22><path d=\x22M0 0h\
24v24H0z\x22 fill=\x22\
none\x22/><path d=\x22\
M12 2C6.48 2 2 6\
.48 2 12s4.48 10\
 10 10 10-4.48 1\
0-10S17.52 2 12 \
2zm1 15h-2v-6h2v\
6zm0-8h-2V7h2v2z\
\x22 fill=\x22white\x22/>\
</svg>\
\x00\x00\x00\xba\
<\
svg xmlns=\x22http:\
//www.w3.org/200\
0/svg\x22 height=\x222\
4\x22 viewBox=\x220 0 \
24 24\x22 width=\x2224\
\x22><path d=\x22M0 0h\
24v24H0z\x22 fill=\x22\
none\x22/><path d=\x22\
M10 20v-6h4v6h5v\
-8h3L12 3 2 12h3\
v8z\x22 fill=\x22white\
\x22/></svg>\
\x00\x00\x00\xc4\
<\
svg xmlns=\x22http:\
//www.w3.org/200\
0/svg\x22 height=\x222\
4\x22 viewBox=\x220 0 \
24 24\x22 width=\x2224\
\x22><path d=\x22M0 0h\
24v24H0z\x22 fill=\x22\
none\x22/><path d=\x22\
M3 18h18v-2H3v2z\
m0-5h18v-2H3v2zm\
0-7v2h18V6H3z\x22 f\
ill=\x22white\x22/></s\
vg>\
\x00\x00\x04\x1c\
<\
svg xmlns=\x22http:\
//www.w3.org/200\
0/svg\x22 enable-ba\
ckground=\x22new 0 \
0 24 24\x22 height=\
\x2224\x22 viewBox=\x220 \
0 24 24\x22 width=\x22\
24\x22><g><path d=\x22\
M0,0h24v24H0V0z\x22\
 fill=\x22none\x22/><p\
ath d=\x22M19.14,12\
.94c0.04-0.3,0.0\
6-0.61,0.06-0.94\
c0-0.32-0.02-0.6\
4-0.07-0.94l2.03\
-1.58c0.18-0.14,\
0.23-0.41,0.12-0\
.61 l-1.92-3.32c\
-0.12-0.22-0.37-\
0.29-0.59-0.22l-\
2.39,0.96c-0.5-0\
.38-1.03-0.7-1.6\
2-0.94L14.4,2.81\
c-0.04-0.24-0.24\
-0.41-0.48-0.41 \
h-3.84c-0.24,0-0\
.43,0.17-0.47,0.\
41L9.25,5.35C8.6\
6,5.59,8.12,5.92\
,7.63,6.29L5.24,\
5.33c-0.22-0.08-\
0.47,0-0.59,0.22\
L2.74,8.87 C2.62\
,9.08,2.66,9.34,\
2.86,9.48l2.03,1\
.58C4.84,11.36,4\
.8,11.69,4.8,12s\
0.02,0.64,0.07,0\
.94l-2.03,1.58 c\
-0.18,0.14-0.23,\
0.41-0.12,0.61l1\
.92,3.32c0.12,0.\
22,0.37,0.29,0.5\
9,0.22l2.39-0.96\
c0.5,0.38,1.03,0\
.7,1.62,0.94l0.3\
6,2.54 c0.05,0.2\
4,0.24,0.41,0.48\
,0.41h3.84c0.24,\
0,0.44-0.17,0.47\
-0.41l0.36-2.54c\
0.59-0.24,1.13-0\
.56,1.62-0.94l2.\
39,0.96 c0.22,0.\
08,0.47,0,0.59-0\
.22l1.92-3.32c0.\
12-0.22,0.07-0.4\
7-0.12-0.61L19.1\
4,12.94z M12,15.\
6c-1.98,0-3.6-1.\
62-3.6-3.6 s1.62\
-3.6,3.6-3.6s3.6\
,1.62,3.6,3.6S13\
.98,15.6,12,15.6\
z\x22 fill=\x22white\x22/\
></g></svg>\
"

qt_resource_name = b"\
\x00\x05\
\x00o\xa6S\
\x00i\
\x00c\x00o\x00n\x00s\
\x00\x08\
\x04\xd2T\xc7\
\x00i\
\x00n\x00f\x00o\x00.\x00s\x00v\x00g\
\x00\x08\
\x068W'\
\x00h\
\x00o\x00m\x00e\x00.\x00s\x00v\x00g\
\x00\x08\
\x0cXT\xa7\
\x00m\
\x00e\x00n\x00u\x00.\x00s\x00v\x00g\
\x00\x0b\
\x00\xbd\xcd\xa7\
\x00s\
\x00e\x00t\x00t\x00i\x00n\x00g\x00.\x00s\x00v\x00g\
"

qt_resource_struct = b"\
\x00\x00\x00\x00\x00\x02\x00\x00\x00\x01\x00\x00\x00\x01\
\x00\x00\x00\x00\x00\x00\x00\x00\
\x00\x00\x00\x00\x00\x02\x00\x00\x00\x01\x00\x00\x00\x02\
\x00\x00\x00\x00\x00\x00\x00\x00\
\x00\x00\x00\x00\x00\x02\x00\x00\x00\x04\x00\x00\x00\x03\
\x00\x00\x00\x00\x00\x00\x00\x00\
\x00\x00\x00R\x00\x00\x00\x00\x00\x01\x00\x00\x02\x81\
\x00\x00\x01y\xb8\xa3}K\
\x00\x00\x00\x10\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\
\x00\x00\x01y\xb8\xa2\x06\x87\
\x00\x00\x00&\x00\x00\x00\x00\x00\x01\x00\x00\x00\xfb\
\x00\x00\x01y\xb8\xa7\xc5\x1d\
\x00\x00\x00<\x00\x00\x00\x00\x00\x01\x00\x00\x01\xb9\
\x00\x00\x01y\xb8\xa2\xf3\xec\
"

def qInitResources():
    QtCore.qRegisterResourceData(0x03, qt_resource_struct, qt_resource_name, qt_resource_data)

def qCleanupResources():
    QtCore.qUnregisterResourceData(0x03, qt_resource_struct, qt_resource_name, qt_resource_data)

qInitResources()
