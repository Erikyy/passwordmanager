from PyQt5 import QtWidgets, QtCore, QtGui
from PyQt5.QtCore import QPropertyAnimation
import sys
import atexit
from gui import Ui_MainWindow
from stylesheets import Stylesheets
from passwordutil import PasswordUtil
from configutils import ConfigUtils
from os.path import expanduser
import about
import passwordutil

password_util = PasswordUtil()
config_utils = ConfigUtils()
class SetupUI(QtWidgets.QMainWindow, Ui_MainWindow):
    
    def __init__(self, parent=None):
        super(SetupUI, self).__init__(parent)
        self.setupUi(self)
        self.stylesheets = Stylesheets()
        self.Toggle_Btn.clicked.connect(lambda: self.toggle(200, True))
        self.Toggle_Btn.setIcon(QtGui.QIcon('icons/menu.svg'))
        self.Toggle_Btn.setIconSize(QtCore.QSize(28, 28))
        self.AddPasswordBtn.clicked.connect(self.add_password)
        self.about_content.setReadOnly(True)
        self.about_content.append(about.about)
        
        self.master_key_config.setEchoMode(QtWidgets.QLineEdit.Password)
        self.purge_btn.clicked.connect(self.purge)
        self.master_key_save.clicked.connect(self.change_master_key)
        self.dir_btn_save.clicked.connect(self.change_path)

        self.menu()
        
        
    def toggle(self, w, enabled):
        if enabled:
            width = self.side_bar.width()
            maxWidth = w
            normal = 70

            if width == 70:
                side_bar_width = maxWidth
            else:
                side_bar_width = normal
            self.animation = QPropertyAnimation(self.side_bar, b"minimumWidth")
            self.animation.setDuration(200)
            self.animation.setStartValue(width)
            self.animation.setEndValue(side_bar_width)
            self.animation.setEasingCurve(QtCore.QEasingCurve.InOutQuint)
            self.animation.start()

    def menu(self):
        self.Home_Btn.clicked.connect(lambda: self.home_page())
        self.About_btn.clicked.connect(lambda: self.about_page())
        self.settings_btn.clicked.connect(lambda: self.settings_page())

        
    def home_page(self):
        if (self.Home_Btn.clicked):
            self.Pages.setCurrentWidget(self.home)
            self.Home_Btn.setStyleSheet(self.stylesheets.home_active())
            self.About_btn.setStyleSheet(self.stylesheets.about())
            self.settings_btn.setStyleSheet(self.stylesheets.settings())
    
    def about_page(self):
        if (self.About_btn.clicked):
            self.Pages.setCurrentWidget(self.about)
            self.Home_Btn.setStyleSheet(self.stylesheets.home())
            self.About_btn.setStyleSheet(self.stylesheets.about_active())
            self.settings_btn.setStyleSheet(self.stylesheets.settings())
            
    
    def settings_page(self):
        if (self.settings_btn.clicked):
            self.Pages.setCurrentWidget(self.settings)
            self.Home_Btn.setStyleSheet(self.stylesheets.home())
            self.About_btn.setStyleSheet(self.stylesheets.about())
            self.settings_btn.setStyleSheet(self.stylesheets.settings_active())
        
    def list_passwords(self):
        self.model = QtGui.QStandardItemModel()
        self.passwords.setModel(self.model)
        
        for test in password_util.password_list():
            item = QtGui.QStandardItem(test[0])
            
            self.model.appendRow(item)

        self.passwords.clicked.connect(self.clicked_item)
    
    def clicked_item(self, index):
        passwordDialog = PasswordDialog(self.model, index.row(), password_util)
        passwordDialog.exec()

    def add_password(self):
        add_psw = AddPasswordDialog(self.model, password_util)
        add_psw.exec()

    def purge(self):
        self.model.clear()
        passwordutil.passwords = []
    
    def change_master_key(self):
        key = self.master_key_config.text()
        password_util.changeMasterKey(key)

    def change_path(self):
        new_dir = self.directory_config.text()
        passwordutil.fileutils.change_type("PATH", "path", new_dir)

    def set_config(self):
        self.directory_config.setText(passwordutil.fileutils.read_type("PATH", "path"))
        self.master_key_config.setText(password_util.getMasterKey())

class PasswordDialog(QtWidgets.QDialog):
    def __init__(self, model: QtGui.QStandardItemModel, index, password_util: PasswordUtil, parent=None):
        super(PasswordDialog, self).__init__(parent)
        self.model = model
        self.index = index
        self.password_util = password_util
        self.setWindowTitle("Password")
        self.password_name = QtWidgets.QLineEdit(self)
        self.password_name.setText(self.password_util.password_list()[index][0])
        self.password = QtWidgets.QLineEdit(self)
        self.password.setText(self.password_util.decrypt(passwordutil.passwords[index][1], passwordutil.passwords[index][2]))
        self.layout = QtWidgets.QVBoxLayout(self)
        self.init_name_layout()
        self.init_password_layout()
        self.init_buttons_layout()

    def onSaveClicked(self):
        self.model.setItem(self.index, QtGui.QStandardItem(self.password_name.text()))
        self.password_util.save(self.index, self.password_name.text(), self.password.text())
        self.accept()

    def onCloseClicked(self):
        self.reject()

    def onDeleteClicked(self):
        self.model.removeRow(self.index)
        self.password_util.delete(self.index)
        self.accept()

    def init_name_layout(self):
        self.label_name = QtWidgets.QLabel(self)
        self.label_name.setText("Name: ")
        self.name_layout = QtWidgets.QHBoxLayout()
        self.name_layout.addWidget(self.label_name)
        self.name_layout.addWidget(self.password_name)
        self.layout.addLayout(self.name_layout)

    def init_password_layout(self):
        self.label_password = QtWidgets.QLabel(self)
        self.label_password.setText("Password: ")
        self.password_layout = QtWidgets.QHBoxLayout()
        self.password_layout.addWidget(self.label_password)
        self.password_layout.addWidget(self.password)
        self.layout.addLayout(self.password_layout)

    def init_buttons_layout(self):
        self.button_layout = QtWidgets.QHBoxLayout()
        self.btn_save = QtWidgets.QPushButton("save")
        self.btn_delete = QtWidgets.QPushButton("delete")
        self.btn_close = QtWidgets.QPushButton("close")
        self.btn_save.clicked.connect(self.onSaveClicked)
        self.btn_delete.clicked.connect(self.onDeleteClicked)
        self.btn_close.clicked.connect(self.onCloseClicked)
        self.button_layout.addWidget(self.btn_save)
        self.button_layout.addWidget(self.btn_delete)
        self.button_layout.addWidget(self.btn_close)
        self.layout.addLayout(self.button_layout)

class AddPasswordDialog(QtWidgets.QDialog):
    def __init__(self, model: QtGui.QStandardItemModel, password_util: PasswordUtil, parent=None):
        super(AddPasswordDialog, self).__init__(parent)
        self.model = model
        self.password_util = password_util
        self.setWindowTitle("Password")
        self.password_name = QtWidgets.QLineEdit(self)
        self.password = QtWidgets.QLineEdit(self)
        self.password.setEchoMode(QtWidgets.QLineEdit.Password)
        self.layout = QtWidgets.QVBoxLayout(self)
        self.init_password_name_layout()
        self.init_password_layout()
        self.init_buttons_layout()

    def init_password_name_layout(self):
        self.label_name = QtWidgets.QLabel(self)
        self.label_name.setText("Name: ")
        self.name_layout = QtWidgets.QHBoxLayout()
        self.name_layout.addWidget(self.label_name)
        self.name_layout.addWidget(self.password_name)
        self.layout.addLayout(self.name_layout)
    
    def init_password_layout(self):
        self.label_password = QtWidgets.QLabel(self)
        self.label_password.setText("Password: ")
        self.password_layout = QtWidgets.QHBoxLayout()
        self.password_layout.addWidget(self.label_password)
        self.password_layout.addWidget(self.password)
        self.layout.addLayout(self.password_layout)
    
    def init_buttons_layout(self):
        self.button_layout = QtWidgets.QHBoxLayout()
        self.btn_save = QtWidgets.QPushButton("save")
        self.btn_close = QtWidgets.QPushButton("cancel")
        self.btn_save.clicked.connect(self.onSaveClicked)
        self.btn_close.clicked.connect(self.onCancelClicked)
        self.button_layout.addWidget(self.btn_save)
        self.button_layout.addWidget(self.btn_close)
        self.layout.addLayout(self.button_layout)

    def onSaveClicked(self):
        self.password_util.add(self.password_name.text(), self.password.text())
        self.model.appendRow(QtGui.QStandardItem(self.password_name.text()))
        self.accept()

    def onCancelClicked(self):
        self.reject()

class LoginDialog(QtWidgets.QDialog):
    def __init__(self, parent=None):
        super(LoginDialog, self).__init__(parent)
        self.password_util = PasswordUtil()
        self.setWindowTitle("Login")
        self.master_key = QtWidgets.QLineEdit(self)
        self.master_key.setEchoMode(QtWidgets.QLineEdit.Password)
        self.btn_verify = QtWidgets.QPushButton('verify', self)
        self.btn_verify.clicked.connect(self.handle)
        self.layout = QtWidgets.QVBoxLayout(self)
        self.layout.addWidget(self.master_key)
        self.layout.addWidget(self.btn_verify)
        
    def handle(self):
        password_util.setMasterPassword(self.master_key.text())
        if password_util.verify(self.master_key.text().encode('utf-8')) == None:
            self.accept()
        
        else:
            QtWidgets.QMessageBox.warning(self, 'Error', 'Wrong key!')


class CreateMasterKeyDialog(QtWidgets.QDialog):
    def __init__(self, parent=None):
        super(CreateMasterKeyDialog, self).__init__(parent)
        self.password_util = PasswordUtil()
        self.setWindowTitle("Add master key")
        self.master_key = QtWidgets.QLineEdit(self)
        self.master_key.setEchoMode(QtWidgets.QLineEdit.Password)
        self.btn_save = QtWidgets.QPushButton('save', self)
        self.btn_save.clicked.connect(self.handle)
        self.layout = QtWidgets.QVBoxLayout(self)
        self.layout.addWidget(self.master_key)
        self.layout.addWidget(self.btn_save)

    def handle(self):
        password_util.setMasterPassword(self.master_key.text())
        password_util.setMasterKey(self.master_key.text())
        self.accept()

class CreateConfDialog(QtWidgets.QDialog):
    def __init__(self, parent=None):
        super(CreateConfDialog, self).__init__(parent)
        self.password_util = PasswordUtil()
        self.setWindowTitle("Add path to config")
        self.path = QtWidgets.QLineEdit(self)
        self.btn_save = QtWidgets.QPushButton('save', self)
        self.btn_save.clicked.connect(self.handle)
        self.layout = QtWidgets.QVBoxLayout(self)
        self.layout.addWidget(self.path)
        self.layout.addWidget(self.btn_save)

    def handle(self):
        if not self.path.text() == "":
            config_utils.create_path(self.path.text())
        else:
            config_utils.create_path(expanduser("~"))
        self.accept()

class GUI():
    def __init__(self):
        
        self.app = QtWidgets.QApplication(sys.argv)
        
        self.window = SetupUI()
        
        atexit.register(password_util.write_file_and_encrypt)
    def run(self):
        if config_utils.config_exists():
            if config_utils.saltExists():
                self.loginkey()
            else:
                self.setupkey()
        else:
            self.setup_conf()
            
        
    def loginkey(self):
        login = LoginDialog()
        if (login.exec() == QtWidgets.QDialog.Accepted):
            self.window.set_config()
            password_util.read_json_decrypted()
            self.window.list_passwords()
            self.window.show()
            self.app.exec_()

    def setupkey(self):
        createMasterKeyDialog = CreateMasterKeyDialog()
        if (createMasterKeyDialog.exec() == QtWidgets.QDialog.Accepted):
            self.window.set_config()
            
            self.window.show()
            self.app.exec_()
    
    def setup_conf(self):
        createConfDialog = CreateConfDialog()
        if (createConfDialog.exec() == QtWidgets.QDialog.Accepted):
            self.window.set_config()
            if config_utils.saltExists():
                self.loginkey()
            else:
                self.setupkey()
            #self.window.show()
            self.app.exec_()
 