import sys, platform
import os
import curses
from curses import panel
from pwgui import GUI


#
#
#   MAIN
#
class App():
    def __init__(self):
        if len(sys.argv) < 2:
            print("not enough arguments")
            try:
                sys.exit(0)
            except SystemExit:
                os._exit(0)
    
        else:
            if sys.argv[1] == "--gui":
                self.gui = GUI()

    def run(self):
        self.gui.run()


if __name__ == '__main__':
    app = App()
    app.run()
    
    