
about = """ About password manager
A simple password manager made with python

License: GNU GPLv3
https://gitlab.com/Erikyy/passwordmanager/-/blob/master/LICENSE

Uses AES-128 to encrypt passwords. 
Master key is saved as a SHA3-256 hash with PBKDF2 (Password Based Key Derivation Function 2) in a file and
passwords are saved to a encrypted file
"""