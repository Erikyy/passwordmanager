import os
import base64
import json
import time
from cryptography.hazmat.primitives.kdf.scrypt import Scrypt
from cryptography.hazmat.primitives import hashes, hmac
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
from cryptography.fernet import Fernet
from fileutils import FileUtils
from configutils import ConfigUtils

passwords = []
fileutils = FileUtils()
key = ""
class PasswordUtil():
    def __init__(self):
        self.key = ""
        self.config_utils = ConfigUtils()

    def encrypt(self, password:bytes, salt):
        kdf = PBKDF2HMAC(hashes.SHA3_256(), 32, salt, 100000)
        fernet = Fernet(base64.urlsafe_b64encode(kdf.derive(self.key.encode('utf-8'))))
        _hash = fernet.encrypt(password)
        return _hash

    def decrypt(self, _hash: bytes, salt):
        kdf = PBKDF2HMAC(hashes.SHA3_256(), 32, salt, 100000)
        fernet = Fernet(base64.urlsafe_b64encode(kdf.derive(self.key.encode('utf-8'))))
        decrypted = fernet.decrypt(_hash)
        return decrypted.decode('utf-8')
    
    
    def verify(self, key) -> None:
        kdf = PBKDF2HMAC(hashes.SHA3_256(), 32, fileutils.read_salt(), 100000)
        kdf.verify(key, base64.urlsafe_b64decode(fileutils.read_hash()))
           
    def changeMasterKey(self, key):
        tmp_list = []
        for i in range(0, len(passwords)):
            decrypted = self.decrypt(passwords[i][1], passwords[i][2])
            tmp_list.append(decrypted)
            
        self.setMasterKey(key)
        self.setMasterPassword(key)
        print("New key set")
        for i in range(0, len(passwords)):
            temporary_password = tmp_list[i]
            salt = os.urandom(16)
            encryted = self.encrypt(temporary_password.encode('utf-8'), salt)
            passwords[i] = (passwords[i][0], encryted, salt)

        tmp_list = []

    #temporary inmemory stored password, can be made more secure by asking user to enter master key at each decryption
    def setMasterPassword(self, key):
        self.key = key

    def getMasterKey(self):
        return self.key

    def setMasterKey(self, key):
        if self.config_utils.saltExists():
            os.remove('./salt.enc')
            os.remove('./hash.enc')
            time.sleep(1)
            
        salt = os.urandom(16)
        kdf = PBKDF2HMAC(hashes.SHA3_256(), 32, salt, 100000)
        master_key = base64.urlsafe_b64encode(kdf.derive(key.encode('utf-8')))
        fileutils.write_salt(salt)
        fileutils.write_hash(master_key)
     
    def add(self, name, password: str):
        salt = os.urandom(16)
        passwords.append((name, self.encrypt(password.encode('utf-8'), salt), salt))

    def save(self, index, name, password: str):
        salt = os.urandom(16)
        passwords[index] = (name, self.encrypt(password.encode('utf-8'), salt), salt)

    def delete(self, index):
        del passwords[index]

    def password_list(self):
        return passwords

    def print_psw(self):
        print(self.password_list())
    
    def write_file_and_encrypt(self):
        fileutils.delete_file(fileutils.read_type("PATH", "path"), "/data.json")
        time.sleep(1)
        json_array = []
        json_array = [{ "name": password[0], "password": password[1].decode('utf-8'), "salt": base64.b64encode(password[2]).decode('utf-8') } for password in passwords]

        passwors_dictionarty = {
            'passwords': json_array
        }
        fileutils.write_to_file(passwors_dictionarty, fileutils.read_type("PATH", "path"), "/data.json")
        kdf = PBKDF2HMAC(hashes.SHA3_256(), 32, fileutils.read_salt(), 100000)
        fernet = Fernet(base64.urlsafe_b64encode(kdf.derive(self.key.encode('utf-8'))))
        data = fileutils.read_file(fileutils.read_type("PATH", "path"), "/data.json")
        enc = fernet.encrypt(data)
        fileutils.encrypt_file(fileutils.read_type("PATH", "path"), "/data.json", enc)
    
    def read_json_decrypted(self):
        kdf = PBKDF2HMAC(hashes.SHA3_256(), 32, fileutils.read_salt(), 100000)
        fernet = Fernet(base64.urlsafe_b64encode(kdf.derive(self.key.encode('utf-8'))))
        enc = fileutils.read_file(fileutils.read_type("PATH", "path"), "/data.json")
        decrypted = fernet.decrypt(enc)
        fileutils.decrypt_file(fileutils.read_type("PATH", "path"), "/data.json", decrypted)
        data = fileutils.read_json_file(fileutils.read_type("PATH", "path"), "/data.json")
        for entry in data['passwords']:
            passwords.append((entry['name'], bytes(entry['password'], 'utf-8'), base64.b64decode(entry['salt'])))
        